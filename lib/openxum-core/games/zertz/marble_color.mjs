"use strict";

const MarbleColor = {NONE: -1, BLACK: 0, WHITE: 1, GREY: 2};

export default MarbleColor;