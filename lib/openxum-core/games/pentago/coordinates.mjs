"use strict";

class Coordinates {
  constructor(l, n) {
    this._letter = l;
    this._number = n;
  }

// public methods
  clone() {
    return new Coordinates(this._letter, this._number);
  }

  hash() {
    return (this._letter.charCodeAt(0) - 'A'.charCodeAt(0)) + (this._number - 1) * 6;
  }

  is_valid() {
    const l = this._letter.charCodeAt(0) - 'a'.charCodeAt(0) + 1;

    return (l >= 1 && l <= 6 && this._number >= 1 && this._number <= 6);
  }

  letter() {
    return this._letter;
  }

  number() {
    return this._number;
  }

  to_string() {
    return this._letter + this._number;
  }
}

export default Coordinates;